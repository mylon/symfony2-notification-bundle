<?php

namespace JnyStudio\Common\NotificationCenterBundle\Exception;

use JnyECS\JobStationCoreBundle\Exception\ApplicationException;

class DeviceException extends ApplicationException {
	
}