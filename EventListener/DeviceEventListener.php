<?php
/**
 * Created by PhpStorm.
 * User: domehuhu
 * Date: 2/21/14 AD
 * Time: 1:43 PM
 */

namespace JnyStudio\Common\NotificationCenterBundle\EventListener;


use DABSquared\PushNotificationsBundle\Event\DeviceEvent;
use JnyStudio\Common\NotificationCenterBundle\Service\DeviceManager;

class DeviceEventListener {

    /**
     * @var DeviceManager
     */
    private $dm;

    public function __construct(DeviceManager $dm) {
        $this->dm = $dm;
    }
    /**
     * dab_push_notifications.device.post_persist
     * @param DeviceEvent $event
     */
    public function onDevicePostPersist(DeviceEvent $event) {
        $device = $event->getDevice();
        // remove all notification devices that have the same user_id, app_id
        $this->dm->removeOtherUserDevices($device);
    }
} 