<?php

namespace JnyStudio\Common\NotificationCenterBundle\Service;

use DABSquared\PushNotificationsBundle\Entity\Device;
use DABSquared\PushNotificationsBundle\Entity\DeviceManager as BaseDeviceManager;
use DABSquared\PushNotificationsBundle\Model\DeviceInterface;
use DABSquared\PushNotificationsBundle\Model\UserDeviceInterface;

class DeviceManager extends BaseDeviceManager {

	public function findByDeviceAndAppId($udid, $appid) {
		return $this->repository->findOneBy(
				array(
						"deviceIdentifier" => $udid, 
						"appId" => $appid
		));
	}
	
	public function findByDeviceToken($deviceToken) {
		return $this->repository->findOneBy(
				array(
						"deviceToken" => $deviceToken,
				));
	}
	
	public function findByUser($user) {
		return $this->repository->findBy(array("user" => $user));
	}

    public function removeOtherUserDevices(DeviceInterface $currentDevice) {
        if ($currentDevice == null || $currentDevice->getId() == null || $currentDevice->getUser() == null)
            return;

        if ($currentDevice instanceof UserDeviceInterface) {
            /* @var $device UserDeviceInterface */
            $q = $this->repository->createQueryBuilder('d')
                ->delete($this->repository->getClassName(), 'd')
                ->where('d.appId = :appId')
                ->andWhere('d.user = :user')
                ->andWhere('d.id != :did')
                ->setParameters([
                    'did' => $currentDevice->getId(),
                    'user' => $currentDevice->getUser(),
                    'appId' => $currentDevice->getAppId()
                ])
                ->getQuery();
            $q->useQueryCache(true);
            $q->execute();
        }
    }

}