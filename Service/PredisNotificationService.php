<?php
namespace JnyStudio\Common\NotificationCenterBundle\Service;

/**
 * @author p_srichuwong
 */
class PredisNotificationService implements NotificationServiceInterface {

	private $predis;
	
	private $setName;
	
	/**
	 */
	public function __construct($redis) {
		$this->predis =  $redis; //$this->container->get('snc_redis.default');
		$this->setName = "Notification_User";
	}
	
	public function createKey($uid, $key) {
		return $rediskey = "notification:" . $uid . ":" . $key;
	}
	
	public function hasNotification($uid, $key) {
		return $this->predis->exists($this->createKey($uid, $key));
	}
	
	public function setNotification($uid, $key, $value) {
		$rkey = $this->createKey($uid, $key);
		$this->predis->sadd($this->setName,$rkey);
		$this->predis->set($rkey, $value);
	}
	
	public function incrNotification($uid, $key) {
		$rkey = $this->createKey($uid, $key);
		$this->predis->sadd($this->setName,$rkey);
		if(!$this->hasNotification($uid, $key)){
			$this->predis->set($rkey, 1);
		}else{
			$this->predis->incr($rkey);
		}
	}
	
	public function decrNotification($uid, $key, $decr = null) {
		$rkey = $this->createKey($uid, $key);
		if(!$this->hasNotification($uid, $key)){
			if(!is_null($decr) && is_numeric($decr))
				$this->predis->decrby($rkey,$decr);
			else
				$this->predis->decr($rkey);
		}
	} 
	
	public function getNotification($uid, $key, $default = null) {
		if (!$this->hasNotification($uid, $key)) {
			return $default;
		}
		
		return $this->predis->get($this->createKey($uid, $key));
	}
	
	public function flushNotifications($uid, $key = null) {
		if ($key = null) {
			// delete all notification
		}else{
			$this->predis->delete($this->createKey($uid, $key));
		}
		
		
	}
}
