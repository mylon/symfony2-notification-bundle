<?php
namespace JnyStudio\Common\NotificationCenterBundle\Service;

use Doctrine\ORM\EntityManager;
use JnyStudio\Common\NotificationCenterBundle\Entity\Device;
use JnyStudio\Common\NotificationCenterBundle\Entity\Message;
use DABSquared\PushNotificationsBundle\Service\Notifications;
use Symfony\Component\Security\Core\User\UserInterface;
use DABSquared\PushNotificationsBundle\Device\Types;
use DABSquared\PushNotificationsBundle\Model\MessageInterface;
use DABSquared\PushNotificationsBundle\Entity\MessageManager;
use DABSquared\PushNotificationsBundle\Message\MessageStatus;


/**
 * @author p_srichuwong
 */
class APNSService {
	
	CONST MESSAGE_NEW_CAMPAIGN_PUBLISHED = "";//"New Compaign release might be interested by you!!";
	
	/**
	 * @var Notifications
	 */
	private $sender;
	
	private $doctrine;
	
	/**
	 * @var MessageManager
	 */
	private $messageManager;
	
	public function __construct($doctrine, $sender, $messageManager) {
		$this->sender = $sender;
		$this->doctrine = $doctrine;
		$this->messageManager = $messageManager;
	}
	
	public function getDevicesByUser($user){
		$em = $this->doctrine->getManager();
		$devices =  $em->getRepository("JnyStudio\Common\NotificationCenterBundle\Entity\Device")
			->findBy(array('user' => $user));
		return $devices;
	}
	
	public function getDevicesByUserId($userid){
		$em = $this->doctrine->getManager();
		$user = $em->getRepository("JnyECS\JobStationCoreBundle\Entity\HomeUser")->find($userid);
		return $this->getDevicesByUser($user);
	}
	
	
	/**
	 * @param $text
	 * @param UserInterface $user
	 * @param $os check DABSquared\PushNotificationsBundle\Device\Types for the availible OS
	 */
	public function sendToUser(UserInterface $user, $text = "", $os = array()) {
		$devices = $this->getDevicesByUser($user);
		foreach ($devices as $d) {
			$this->sendToDevice($d, $text);
		}
	}
	
	public function sendToUserId($userId, $text = "", $os = array()) {
		$devices = $this->getDevicesByUserId($userId);
		foreach ($devices as $d) {
			$this->sendToDevice($d, $text);
		}
	}
	
	public function sendToDevice($device, $text = "") {
		/* @var Device */
		$msg = new Message();
		$msg->setMessage($text);
		$msg->setDevice($device);
		$this->sender->send($msg);
		
		return $msg;
	}
	
	public function saveMessages(MessageInterface $msg) {
		$this->messageManager->saveMessage($msg);
	}
	
	public function cleanupSentMessages() {
		$query = $this->doctrine->getManager()->createQuery(
				"DELETE FROM JnyStudio\Common\NotificationCenterBundle\Entity\Message m
				WHERE m.status = :sentStatus")
			->setParameter("sentStatus", MessageStatus::MESSAGE_STATUS_SENT);
		/* @var $query \Doctrine\ORM\Query */
		$query->execute();
	}
}
