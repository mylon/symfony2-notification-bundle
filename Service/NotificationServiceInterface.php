<?php
namespace JnyStudio\Common\NotificationCenterBundle\Service;

interface NotificationServiceInterface {
	
	public function hasNotification($uid, $key);
	
	public function setNotification($uid, $key, $value);
	
	public function getNotification($uid, $key, $default = "");
	
	public function flushNotifications($uid, $key = null);	
}
