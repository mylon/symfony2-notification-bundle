<?php

namespace JnyStudio\Common\NotificationCenterBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanSentPushMessagesCommand  extends ContainerAwareCommand {

	/**
	 * @see Command
	 */
	protected function configure()
	{
		$this->setName('notification:apns:cleanup');
	}

	/**
	 * @see Command
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$apns = $this->getContainer()->get('jnystudio.push_notification_service');
		$apns->cleanupSentMessages();
	}
}
