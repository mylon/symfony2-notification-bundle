<?php
namespace JnyStudio\Common\NotificationCenterBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use DABSquared\PushNotificationsBundle\Entity\Device as BaseDevice;
use Doctrine\ORM\Mapping as ORM;
use DABSquared\PushNotificationsBundle\Model\UserDeviceInterface;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * @ORM\Entity
 * @ORM\Table("notification_devices")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @ORM\HasLifecycleCallbacks()
 */
class Device extends BaseDevice implements UserDeviceInterface {

	/**
	 * Owner of the device
	 *
	 * @ORM\ManyToOne(targetEntity="JnyECS\JobStationCoreBundle\Entity\User")
	 * @var User
	 */
	protected $user;	
	
	/**
	 * @ORM\OneToMany(targetEntity="Message", mappedBy="device", cascade={"remove"})
	 */
	protected $messages;


	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->messages = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * @ORM\PrePersist
	 */
	public function PrePersist() {
		$this->createdAt = new \DateTime();
		$this->updatedAt = new \DateTime();

	}

	/**
	 * @ORM\PreUpdate
	 */
	public function PreUpdate() {
		$this->updatedAt = new \DateTime();
	}

	/**
	 * Add message
	 * @return Device
	 */
	public function addMessage($message)
	{
		$this->messages[] = $message;

		return $this;
	}

	/**
	 * Remove message
	 */
	public function removeMessage($message)
	{
		$this->messages->removeElement($message);
	}

	/**
	 * Get messages
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getMessages()
	{
		return $this->messages;
	}
	
	public function setUser(UserInterface $user)
	{
		$this->user = $user;
	}
	
	public function getUser()
	{
		return $this->user;
	}	
}
