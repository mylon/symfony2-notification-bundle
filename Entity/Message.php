<?php
namespace JnyStudio\Common\NotificationCenterBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use DABSquared\PushNotificationsBundle\Entity\Message as BaseMessage;

/**
 * @ORM\Entity
 * @ORM\Table("notification_messages")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @ORM\HasLifecycleCallbacks()
 */
class Message extends BaseMessage {

	public function __construct() {

	}

	/**
	 * @ORM\PrePersist
	 */
	public function PrePersist() {
		$this->createdAt = new \DateTime();
		$this->updatedAt = new \DateTime();

	}

	/**
	 * @ORM\PreUpdate
	 */
	public function PreUpdate() {
		$this->updatedAt = new \DateTime();
	}

	/**
	 * @ORM\ManyToOne(targetEntity="Device", inversedBy="messages")
     * @ORM\JoinColumn(name="device_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	protected $device;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set device
	 * @return Message
	 */
	public function setDevice($device = null)
	{
		$this->device = $device;
		$this->setTargetOS($device->getType());
		return $this;
	}

	/**
	 * Get device
	 *
	 * @return \DABSquared\PushBundle\Entity\Device
	 */
	public function getDevice()
	{
		return $this->device;
	}

    public function getGCMIdentifiers() {
        return $this->device->getDeviceToken();
    }
}